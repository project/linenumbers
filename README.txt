
-- SUMMARY --

Linenumbers is a field formatter for Long Text fields which adds line
numbering to content.

This module is useful when implementations Syntax highlighter and
Geshi Filter are overkill and all you need is numbered lines for
something simple.

For a full description of the module, visit the project page:
  http://drupal.org/project/linenumbers

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/node/add/project-issue/1789542


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual,
  see http://drupal.org/documentation/install/modules-themes/modules-8
  for further information.


-- CONFIGURATION --

None.

-- CUSTOMIZATION --

None.

-- TROUBLESHOOTING --

* If the formatter does not display, check the following:

  - The module is enabled.

  - The cache has been cleared.


-- CONTACT --

Current maintainers:
* Carwin Young (carwin) - http://drupal.org/user/487058

